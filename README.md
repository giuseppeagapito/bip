# BiP

BioPAX Parser is graphical tool to perform pathway enrichment analysis using 
BioPAX files. In addition BiP enable user to extract and save pathway information 
in a tabular format.

################
BiP BioPAXParser
################

Website: BiP application is available at: https://gitlab.com/giuseppeagapito/bip

--------------------
BiP Library dependencies
--------------------
BiP application encompasses all the dependencies within the provide JAR file, which means you do not need to include any
libraries.

---------
Execution
---------
You do not need to compile BiP in order to execute it, you should be able to run with the simple command:
$java -XMx4G -jar BiP.jar

---------------------
Choosing Java Version
---------------------
To execute BiP using Java 8 or higher, You should refer to the BiP User Manual available at: https://gitlab.com/giuseppeagapito/bip

----------------------
Which are BiP folders?
----------------------
The first time you download data from PathwayCommons using the available BiP function, BiP creates a BiPData folder in your home directory, $HOME$/BiPData 

-------
License
-------
BiP is made available under the LGPL 2.1, the GNU Lesser General Public License, version 2.1, February 1999 available in text at http://www.gnu.org/licenses/lgpl-2.1.html.

-------
Authors
-------
BiP is developed by:
Giuseppe Agapito
